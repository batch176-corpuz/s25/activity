//count total of items supplied by Red Farms Inc.
db.fruits.aggregate([
	{ $match: { supplier: "Red Farms Inc." } },
	{ $count: "itemsRedFarms" },
]);

//count total items with price > 50
db.fruits.aggregate([
	{ $match: { price: { $gt: 50 } } },
	{ $count: "priceGreaterThan50" },
]);

//avg price of fruits onSale per supplier
db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier", avgPricePerSupplier: { $avg: "$price" } } },
]);

//max price of fruits onSale per supplier
db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier", maxPricePerSupplier: { $max: "$price" } } },
]);

//min price of fruits onSale per supplier
db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier", minPricePerSupplier: { $min: "$price" } } },
]);
